/*******************************************************************************************************************************************************************************/
/****************************************************************************** VARIABLES **************************************************************************************/
/*******************************************************************************************************************************************************************************/
let token = document.querySelector('meta[name="customer_first_token"]');

const currentUrl = window.location.href.split('?')[0];

let sites = {
    "http://localhost/extension-formulaire/form1.html": {
        fields: [
            {
                label: "#form_name",
                key: "name",
                content: '',
                type: "text",
            },
            {
                label: "#form_email",
                key: "email",
                content: '',
                type: "text",
            }]
    },

    "http://localhost/extension-formulaire/form2.html": {
        fields: [
            {
                label: "#form_name2",
                key: "name",
                content: '',
                type: "text",
            },
            {
                label: "#form_email2",
                key: "email",
                content: '',
                type: "text",
            }]
    },

    "http://localhost/extension-formulaire/form3.html": {
        fields: [
            {
                label: "#form_name3",
                key: "name",
                content: '',
                type: "text",
            },
            {
                label: "#form_email3",
                key: "email",
                content: '',
                type: "text",
            }]
    },
};

/*******************************************************************************************************************************************************************************/
/********************************************************************************* FUNCTIONS ***********************************************************************************/
/*******************************************************************************************************************************************************************************/

function storageSet(data) {
    chrome.storage.sync.set({
        extension: data
    }, function () {

    });
}

function fillForm(pageFieldsContent) {

    chrome.storage.sync.get(
        ['extension']
        , function (results) {
            let inputIndex = 0;
            for (const oneInputFromStorage of Object.values(results.extension)) {
                if (pageFieldsContent[inputIndex].key === oneInputFromStorage.key) {
                    document.querySelector(pageFieldsContent[inputIndex].label).value = oneInputFromStorage.content;
                }
                inputIndex++;
            }
        })
}

/*******************************************************************************************************************************************************************************/
/********************************************************************************* CODE ****************************************************************************************/
/*******************************************************************************************************************************************************************************/

if (sites[currentUrl]) {

    let fieldsContent = sites[currentUrl].fields;

    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {


        for (const [idKey, idContent] of Object.entries(fieldsContent)) {
            idContent.content = document.querySelector(idContent.label) ? document.querySelector(idContent.label).value : "";
        }
        /***************************************************************************** RETRIEVE TAB MESSAGE ********************************************************************/
        let action = "";
        try {
            action = request.action;
            sendResponse({status: true});
        } catch (error) {
            sendResponse({status: error});
            console.log(error)
        }

        /********************************************************* REGISTER FIELDS OF CURRENT FORM IN STORAGE *******************************************************************/
        if (action === 'load') {
            storageSet(fieldsContent);

            /******************************************************************* FILL FORM FROM STORAGE DATA  *******************************************************************/
        } else if (action === 'autofill') {
            fillForm(fieldsContent);

        }
    });

}


// CODE THIBAUT if (token) {
// CODE THIBAUT     /* je suis dans leads pilot, je store le token en cache local */
// CODE THIBAUT     chrome.storage.local.set({leads_pilot_token: token.content}, function () {
// CODE THIBAUT         console.log('token set');
// CODE THIBAUT     });
// CODE THIBAUT
// CODE THIBAUT } else {
// CODE THIBAUT     /* je suis dans customer first */
// CODE THIBAUT     chrome.storage.local.get(['customer_first_token'], function (result) {
// CODE THIBAUT         token = result.leads_pilot_token;
// CODE THIBAUT         console.log(token);
// CODE THIBAUT     });
// CODE THIBAUT }