'use strict';

/*********************************************************** FUNCTIONS ******************************************************/

function sendTabMessage(message) {
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id,
            message
            , function () {
            });
    });
}

/*********************************************************** CODE ***********************************************************/

document.getElementById("auto-fill").addEventListener("click", function() {
    sendTabMessage({action: "autofill",});
});


document.getElementById("load").addEventListener("click", function()  {
    sendTabMessage({action: 'load',});
});

